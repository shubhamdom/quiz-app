from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [

    path('',views.exam,name="exam"),
    path('newexam/',views.newExam,name="newexam"),
    path('new/',views.new,name='new')
]