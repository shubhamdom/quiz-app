from django.shortcuts import render,redirect
from .models import examdata
from .forms import ExamForm

# Create your views here.
def exam(request):
    data = examdata.objects.all()
    return render(request, 'index.html',{"data": data})


def newExam(request):
    if request.method == 'POST':
        form = ExamForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request,'new.html')
    form = ExamForm()

    return render(request,'examForm.html',{
        'form':form
    })

def new(request):
    if request.method=='post':
        form = ExamForm()

        return render(request, 'examForm.html', {
            'form': form
        })



